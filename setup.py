from distutils.core import setup, Extension

sources = [
	'justextcpp.cpp', 
	'justextcpp/justext.cpp',
	'justextcpp/parseFSM.cpp',
	'justextcpp/tools.cpp']

libraries = [
	'pcrecpp',
	'htmlcxx']

library_dirs = ['.']

include_dirs = ['.']

module = Extension("justextcpp", 
	sources=sources, 
	libraries=libraries, 
	include_dirs=include_dirs,
	library_dirs=library_dirs)

setup(
	name='justextcpp',
	version='0.1',
	description='Justext boilerplate removal algorithm implemented as Python Extension in C++',
	ext_modules=[module])
