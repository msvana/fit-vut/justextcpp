#include <Python.h>
#include <string>
#include <string.h>

#include "justextcpp/justext.h"

Justext * j;
std::string url = "url";

bool keep_everything = false;

static PyObject * justextcpp_justext(PyObject * self, PyObject * args) {
	const char * html_cstring;

	if(!PyArg_ParseTuple(args, "s", &html_cstring))
		return NULL;

	std::string html(html_cstring);
	std::vector<paragraph> paragraphs;

	if(keep_everything)
		paragraphs = j->getAllParagraphs(html, url);
	else
		paragraphs = j->getGoodParagraphs(html, url);

	PyObject * paragraph_list = PyList_New(paragraphs.size());

	for(size_t i=0; i<paragraphs.size(); i++) {
		ReplaceAtoB(paragraphs[i].text, "&nbsp;", " ");
		ReplaceAtoB(paragraphs[i].text, "&quot;", "\"");

		const char * c_string = paragraphs[i].text_with_html.c_str();
		PyObject * paragraph = PyString_FromString(c_string);
		PyList_SetItem(paragraph_list, i, paragraph);
	}

	return paragraph_list;
}

static PyObject * justextcpp_set_modifier(PyObject * self, PyObject * args) {
	PyObject * modifier;

	if(!PyArg_ParseTuple(args, "O", &modifier))
		Py_RETURN_NONE;

	j->m_modifier = modifier;
	Py_RETURN_NONE;
}

static PyObject * justextcpp_keep_everything(PyObject * self, PyObject * args) {
	PyObject * local_keep_everything;

	if(!PyArg_ParseTuple(args, "O", &local_keep_everything))
		Py_RETURN_NONE;

	keep_everything = PyObject_IsTrue(local_keep_everything);
	Py_RETURN_NONE;
}

static PyMethodDef JustextMethods[] = {
	{ "justext", justextcpp_justext, METH_VARARGS, "Remove boilerplate from html string" },
	{ "set_justext_modifier", justextcpp_set_modifier, METH_VARARGS, "Set a function to modify justext output for each paragraph" },
	{ "keep_everything", justextcpp_keep_everything, METH_VARARGS, "Should justext return all paragraphs including bad ones"},
	{ NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initjustextcpp(void) {
	j = new Justext("stoplists/English.txt");
	PyObject * m = Py_InitModule("justextcpp", JustextMethods);
	if(m == NULL) return;
}

int main(int argc, char ** argv) {
	Py_SetProgramName(argv[0]);
	Py_Initialize();
	initjustextcpp();
	return 0;
}
